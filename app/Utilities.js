import axios from 'axios';

export const PROTECTED_SERVER_ROUTE = 'protected';
export const HEARTBEAT_SERVER_ROUTE = 'auth/heartbeat';
export const LOGOUT_SERVER_ROUTE = 'auth/heartbeat';

export function IsAuthenticated() {
    const isSessionInitialized = localStorage.getItem('sessionKey')!==null?true:false;
    const isStillValidTTL = new Date() < new Date(localStorage.getItem('sessionExpires'));

    if (!isSessionInitialized){
        CleanLocalSession();
        return false;
    }
    else if (!isStillValidTTL){  //safe check, is the session still valid on the server?
        axios.post(GetBackendUrl(HEARTBEAT_SERVER_ROUTE), {withCredentials: true}, { headers: { 'Accept': 'application/json' } })
        .then(function (response) {
            isStillValidTTL = response.status===200? true: false;
            if (!isStillValidTTL){
            CleanLocalSession();
            }
            return isStillValidTTL && isSessionInitialized; //TRUE
        })
        .catch(function (error) {
            CleanLocalSession();
            //console.error(error);
            return false;
        });
    }
    else return isStillValidTTL && isSessionInitialized; //TRUE
}


export function CleanLocalSession(){
    localStorage.removeItem('sessionKey');
    localStorage.removeItem('sessionExpires');
}

export function GetFrontendHostname() {
    if (process.env.NODE_ENV==='development')
        return "http://localhost:3000/"
    else if(process.env.NODE_ENV==='production')
        return "https://crescendo-frontend.herokuapp.com/"
}
export function GetBackendHostname() {
    if (process.env.NODE_ENV==='development')
        return "http://localhost:8000/"
    else if(process.env.NODE_ENV==='production')
        return "https://crescendo-backend.herokuapp.com/"
}
export function GetFrontendUrl(subUrl) {
    return GetFrontendHostname()+subUrl;
}
export function GetBackendUrl(subUrl) {
    return GetBackendHostname()+subUrl;
}

export function EncodeForm(data) {
    return Object.keys(data)
    .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(data[key]))
    .join('&');
}