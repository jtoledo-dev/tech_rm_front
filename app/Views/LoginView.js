import React from 'react';
import { Container, Row, Col } from 'reactstrap';

import Nav from '../Components/Nav';
import AuthenticationForm from '../Components/AuthenticationForm';
import LoginForm from '../Components/LoginForm';

export default class LoginView extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
    }

    render(){
        return(
            <Container>
                <Nav/>
                <hr />
                <Row>
                    <Col md="12" >
                        <LoginForm history={this.props.history}/>
                    </Col>
                </Row>
                <hr />
            </Container>
        )
    }
}