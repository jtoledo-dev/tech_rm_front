import React from 'react';
import axios from 'axios';
import { Container, Row, Col, Util } from 'reactstrap';

import Nav from '../Components/Nav';

import * as Utilities from '../Utilities';

import '../index.css';
import UsersGrid from '../Components/UsersGrid';

export default class UserView extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            usersData: []
        };
    }
    
    componentDidMount(){
        if (!Utilities.IsAuthenticated()){
            this.props.history.push('/');
            return;
        }
        const self = this;
        axios.get(Utilities.GetBackendUrl(Utilities.PROTECTED_SERVER_ROUTE), null, { headers: { 'Accept': 'application/json' } })
        .then(function (response) {
            if (response.status===200){
                self.setState({'usersData': response.data.data });
            }
        })
        .catch(function (error) {
            Utilities.CleanLocalSession();
            self.props.history.push('/');
            console.error(error);
        });
    }

    render(){
        return(
            <Container>
                <Nav/>
                <hr />
                <Row>
                    <Col md="12" >
                        Protected View
                        <UsersGrid usersData={this.state.usersData}/>
                    </Col>
                </Row>
                <hr />
                </Container>
        )
    }
}

