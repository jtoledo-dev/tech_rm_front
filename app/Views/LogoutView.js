import React from 'react';
import axios from 'axios';
import * as Utilities from '../Utilities';

export default class LogoutView extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount(){
        const self = this;
        Utilities.CleanLocalSession();
        axios.post(Utilities.GetBackendUrl(Utilities.LOGOUT_SERVER_ROUTE), {}, { headers: { 'Accept': 'application/json' } })
        .then(function (response) {
            self.props.history.push('/');
        })
        .catch(function (error) {
            self.props.history.push('/');
        });
    }
    render(){
        return(
            <></>
        )
    }
}