import React from 'react';
import { Container, Row, Col } from 'reactstrap';

import Nav from '../Components/Nav';

import '../index.css';
import LoginForm from '../Components/LoginForm';
import { IsAuthenticated } from '../Utilities';
export default class MainView extends React.Component{

    componentDidMount(){
        if (IsAuthenticated()){
            this.props.history.push('/user');
        }
    }
    render(){
        return(
            <Container>
                <Nav/>
                <hr />
                <Row>
                    <Col md="12" >
                    <LoginForm history={this.props.history}/>;
                    </Col>
                </Row>
                <hr />
                </Container>
        )
    }
}

