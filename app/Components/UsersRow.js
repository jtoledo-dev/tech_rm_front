import React from 'react';
import { Button, Card, Elevation } from "@blueprintjs/core";
import { Row } from 'reactstrap';
import UserDetail from './UserDetail';

export const MAX_ITEMS = 4;
export default class UsersRow extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render(){
        const usersRow = [];
        let propKey=0;
        this.props.usersData.forEach(userData => {
            propKey++;
            usersRow.push(<UserDetail userData={userData} key={propKey}/>);
        });
        return (
           <Row>
               {usersRow}
           </Row>
        )
    }
}