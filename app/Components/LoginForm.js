import React from 'react';
import AuthenticationForm, { DEFAULT_LOGIN_ENDPOINT } from './AuthenticationForm';
import { Button, Card, Elevation, FormGroup, InputGroup, Intent, Divider } from "@blueprintjs/core";
import { Container, Row, Col } from 'reactstrap';
import { INTENT_PRIMARY } from '@blueprintjs/core/lib/esm/common/classes';
export default class LoginForm extends AuthenticationForm{
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            sessionExpires: "",
            withCredentials: true,
            endpoint: DEFAULT_LOGIN_ENDPOINT
        }
        this.onSucess = this.onSucess.bind(this);
        this.navigateToSignUp = this.navigateToSignUp.bind(this);
        this.navigateToCharacters = this.navigateToCharacters.bind(this);
    }
    onSucess(){
        localStorage.setItem("sessionExpires", this.state.sessionExpires);
        localStorage.setItem("sessionKey", this.state.username);
        this.navigateToCharacters();
    }
    navigateToCharacters() {
        this.props.history.push("/user");
    }
    navigateToSignUp(){
        this.props.history.push("/signup");
    }
    render(){
        return(
            <Card interactive={true} elevation={Elevation.ONE}>
                    <Row>
                        <Col md="12" >
                            <FormGroup
                                label="Username"
                                labelFor="input_username"
                                labelInfo="(required)">
                                <InputGroup
                                    id="input_username"
                                    name="username"
                                    placeholder="Rick Sanchez"
                                    value={this.state.username}
                                    onChange={this.onFormChanged}></InputGroup>
                            </FormGroup>
                            <FormGroup
                                label="Password"
                                labelFor="input_password"
                                labelInfo="(required)">
                                <InputGroup id="input_password"
                                    name="password"
                                    placeholder="***"
                                    value={this.state.password}
                                    onChange={this.onFormChanged}></InputGroup>
                            </FormGroup>     
                            <Button fill="true"
                                onClick={this.onSubmit}>Login</Button>
                        </Col>
                    </Row>
                    <Divider/>
                    <Row>
                        <Col md="12" >
                            <Card interactive={false} elevation={Elevation.ONE}>
                                <h5 className="bp3-heading">Don't have an account?</h5>
                                <Button fill="true" intent={Intent.PRIMARY}
                                    onClick={this.navigateToSignUp}>Signup
                                </Button>
                            </Card>
                        </Col>
                    </Row>
                </Card>
        )
    }
}