import React from 'react';
import { Button, Card, Elevation } from "@blueprintjs/core";
import UserDetail from './UserDetail';
import { Container, Row, Col } from 'reactstrap';
import UsersRow, { MAX_ITEMS } from './UsersRow';

export default class UsersGrid extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
        }
    }

    render(){
        let usersRowsComponents = [];
        let userRowData = [];
        let usersProcessed = 0;
        let propKey=0;
        this.props.usersData.forEach(userData => {
            userRowData.push(userData);
            usersProcessed++;
            propKey++;
            if (usersProcessed===MAX_ITEMS)
            {
                usersRowsComponents.push(<UsersRow usersData={userRowData} key={propKey}/>);
                userRowData=[];
                usersProcessed=0;
            }
            
        });
        let dummyDetails = {
            name: "NAME",
            status: "STATUS",
            species: "SPECIES",
            gender: "GENDER",
            image: "https://via.placeholder.com/150"
        }
        return (
            <div>
                {usersRowsComponents}
            </div>

        )
    }
}