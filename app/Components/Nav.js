import React from 'react';

import { Link } from 'react-router-dom';

import {
    Alignment,
    Button,
    Classes,
    Navbar,
    NavbarDivider,
    NavbarGroup,
    NavbarHeading
} from "@blueprintjs/core";

import '../index.css';
import { IsAuthenticated } from '../Utilities';
export default class Nav extends React.Component{
    render(){
        const loginoutButton = !IsAuthenticated()?
            <Link to="/login">
                <Button className={Classes.MINIMAL} icon="log-in" text="login" />
            </Link>:
            <Link to="/logout">
                <Button className={Classes.MINIMAL} icon="log-out" text="logout" />
            </Link>;
        return(
            <Navbar>
            <NavbarGroup align={Alignment.LEFT}>
                    <NavbarHeading>
                        <b>R&M Front</b>
                    </NavbarHeading>
                <NavbarDivider />
            </NavbarGroup>
            <NavbarGroup align={Alignment.RIGHT}>
                <Link to="/">
                    <Button className={Classes.MINIMAL} icon="home" text="Home" />
                </Link>
                {loginoutButton}
            </NavbarGroup>
        </Navbar>
        )
    }
}
