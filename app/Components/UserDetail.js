import React from 'react';
import { Button, Card, Elevation, Icon } from "@blueprintjs/core";
import { Col } from 'reactstrap';
import '../index.css';
export default class UserDetail extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            userData: {
                name: "",
                status: "",
                species: "",
                gender: "",
                image: ""
            }
        }
    }

    getStatus(status){
        if (status && status!=="unknown"){
        const output = this.props.userData.status==="Alive"?
            <p><strong>Status:</strong> {status} <Icon icon="heart" /></p>:
            <p><strong>Status:</strong> {status} <Icon icon="heart-broken" /></p>;
        return output;
        }
        else
        return <p><strong>Status:</strong> unknown <Icon icon="disable" /></p>;
    }
    getSpecies(species)
    {
        if (species && species!=="unknown"){
            const output = species==="Human"?
            <p><strong>Species:</strong> {species} <Icon icon="person" /></p>:
            <p><strong>Species:</strong> {species} <Icon icon="delta" /></p>;
            return output;
        }
        else
            return <p><strong>Species:</strong> unknown <Icon icon="disable" /></p>;
    }
    getGender(gender){
        if (gender && gender!=="unknown"){
            const output = gender==="Male"?
            <p><strong>Gender:</strong> {gender} <Icon icon="symbol-triangle-up" /></p>:
            <p><strong>Gender:</strong> {gender} <Icon icon="symbol-triangle-down" /></p>;
            return output;
        }
        else
            return <p><strong>Gender:</strong> unknown <Icon icon="disable" /></p>;
    }
    
    render(){
        return (
            <Col md="3">
                <Card interactive={true} elevation={Elevation.TWO} className="top-10">
                    <h5>{this.props.userData.name}</h5>
                    {this.getStatus(this.props.userData.status)}
                    {this.getSpecies(this.props.userData.species)}
                    {this.getGender(this.props.userData.gender)}
                    <img src={this.props.userData.image} className="rounded  img-fluid"/>
                </Card>
            </Col>
        )
    }
}