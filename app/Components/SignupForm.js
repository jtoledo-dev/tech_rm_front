import React from 'react';
import AuthenticationForm, { DEFAULT_SIGNUP_ENDPOINT } from './AuthenticationForm';
import { Button, Card, Elevation, FormGroup, InputGroup, Intent } from "@blueprintjs/core";
import { Container, Row, Col } from 'reactstrap';

export default class SignupForm extends AuthenticationForm{
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            email: "",
            withCredentials: true,
            endpoint: DEFAULT_SIGNUP_ENDPOINT
        }
    }
    render(){
        return(
            <Card interactive={true} elevation={Elevation.ONE}>
                <Row>
                    <Col md="12" >
                        <FormGroup
                            label="Username"
                            labelFor="input_username"
                            labelInfo="(required)">
                            <InputGroup
                                id="input_username"
                                name="username"
                                placeholder="Rick Sanchez"
                                value={this.state.username}
                                onChange={this.onFormChanged}></InputGroup>
                        </FormGroup>
                        <FormGroup
                            label="Email"
                            labelFor="input_email"
                            labelInfo="(required)">
                            <InputGroup id="input_email"
                                name="email"
                                placeholder="rick@morty.com"
                                value={this.state.email}
                                onChange={this.onFormChanged}></InputGroup>
                        </FormGroup>      
                        <FormGroup
                            label="Password"
                            labelFor="input_password"
                            labelInfo="(required)">
                            <InputGroup id="input_password"
                                name="password"
                                placeholder="***"
                                value={this.state.password}
                                onChange={this.onFormChanged}></InputGroup>
                        </FormGroup>     
                        
                        <Button fill="true"
                            onClick={this.onSubmit}>Submit</Button>
                    </Col>
                </Row>
            </Card>
        )
    }
}