import React from 'react';
import axios from 'axios';

import { Container, Row, Col } from 'reactstrap';
import { Button, Card, Elevation, FormGroup, InputGroup, Intent } from "@blueprintjs/core";
import Nav from '../Components/Nav';

import * as Utilities from '../Utilities';

export const DEFAULT_SIGNUP_ENDPOINT = "auth/register";
export const DEFAULT_LOGIN_ENDPOINT = "auth/login";

export default class AuthenticationForm extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
            email: "",
            endpoint: "",
            sessionExpires: ""
        }
        this.onFormChanged = this.onFormChanged.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onSucess = this.onSucess.bind(this);
    }
    onFormChanged(event) {
        this.setState({[event.target.name]: event.target.value});
      }
    onSubmit(event) {
        let self = this;
        axios.post(Utilities.GetBackendUrl(this.state.endpoint), Utilities.EncodeForm(this.state), { headers: { 'Accept': 'application/json',"Access-Control-Allow-Origin":'true' } })
            .then(function (response) {
                if (response.status===200)
                {
                    self.setState({'sessionExpires': response.data.expires});
                    self.props.history.push('/user');
                    self.onSucess();
                }
            })
            .catch(function (error) {
                console.error(error);
            });
        event.preventDefault();
      }
    onSucess(){    }
}