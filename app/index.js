import React from 'react';
import ReactDOM from 'react-dom';

import "@blueprintjs/core/lib/css/blueprint.css";
import "normalize.css";
import "bootstrap/dist/css/bootstrap.min.css";
import './index.css';

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import MainView from './Views/MainView';
import LoginView from './Views/LoginView';
import SignupView from './Views/SignupView';
import UserView from './Views/UserView';
import LogoutView from './Views/LogoutView';

class App extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
    }

    render(){
        return(
            <Router>
                <Switch>
                    <Route path="/" exact component={MainView} />
                    <Route path="/user" exact component={UserView} history={this.props.history}/>
                    <Route path="/login" component={LoginView} history={this.props.history} />
                    <Route path="/signup" component={SignupView} history={this.props.history} />
                    <Route path="/logout" component={LogoutView} history={this.props.history} />
                </Switch>
            </Router>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'))